# PKI/x509 talk

This repo contains slides to my introductory talk on x509 public key infrastructure (PKI). Built using [Inkscape](https://inkscape.org/) and [Sozi](https://sozi.baierouge.fr/).

The slides are served here: [diskordanz.gitlab.io/pki-talk/](https://diskordanz.gitlab.io/pki-talk/)
